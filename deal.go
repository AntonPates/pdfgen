package main

import (
  "./deals/kbgd"
  "strings"
  "net/http"
  "log"
  "encoding/base64"
  "path/filepath"
  "os"
  )


func main() {
  fs := http.StripPrefix("/public/",http.FileServer(http.Dir(filepath.Join(filepath.Dir(os.Args[0]),"public"))))
  http.Handle("/public/",fs)

  http.HandleFunc("/kbgd",logPanics(kbgd.KbgdPage))
  http.HandleFunc("/kbgd/newvaucher",logPanics(kbgd.HandleKbgdData))

  if err := http.ListenAndServe(":9631",nil); err != nil {
    log.Fatal("failed to start server",err)
  }
}

func logPanics(function func(http.ResponseWriter,*http.Request)) func(http.ResponseWriter, *http.Request){

  return func(writer http.ResponseWriter,request *http.Request){
    //request.SetBasicAuth("trivium","trivium")
    if len(request.Header["Authorization"]) == 0 {
      writer.Header().Set("WWW-Authenticate", "Basic")
      writer.WriteHeader(401)
      writer.Write([]byte(`<!DOCTYPE html><title>Unauthorized</title><h1>401 Unauthorized</h1></html>`))
      return
    }
    auth := strings.SplitN(request.Header["Authorization"][0], " ", 2)
    if len(auth) != 2 || auth[0] != "Basic" {
      writer.Header().Set("WWW-Authenticate", "Basic")
      writer.WriteHeader(401)
      writer.Write([]byte(`<!DOCTYPE html><title>Authorization failed</title><h1>401 authorization failed</h1></html>`))
      //http.Error(writer, "bad syntax", http.StatusBadRequest)
      return
    }
    payload, _ := base64.StdEncoding.DecodeString(auth[1])
    pair := strings.SplitN(string(payload), ":", 2)
    if len(pair) != 2 || !validate(pair[0], pair[1]) {
      writer.Header().Set("WWW-Authenticate", "Basic")
      writer.WriteHeader(401)
      writer.Write([]byte(`<!DOCTYPE html><title>Authorization failed</title><h1>401 authorization failed</h1></html>`))
      //http.Error(writer, "authorization failed", http.StatusUnauthorized)
      return
    }

    defer func() {
      if x:=recover(); x != nil {
        log.Printf("[%v] caught panic: %v", request.RemoteAddr,x)
      }
    }()
    function(writer,request)
  }
}

func validate(username, password string) bool {
    if username == "trivium" && password == "trivium" {
        return true
    }
    return false
}
