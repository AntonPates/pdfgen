package kbgd

import (
  "fmt"
  iconv "github.com/djimenez/iconv-go"
  "github.com/jung-kurt/gofpdf"
  "path/filepath"
  "os"
  "strings"
  "net/http"
  "encoding/json"
  "log"
  "time"
  "io/ioutil"
  "html/template"
  "strconv"
  "bytes"
  )

type Category struct {
  Idx uint8
  Value string
  Selected string
}
type Meal struct {
  Idx uint8
  Value string
  Selected string
}
type StartPlace struct {
  Idx uint8
  Value string
  Selected string
}
type Day struct {
  Idx uint8
  Value string
  Selected string
}
type Salesman struct {
  Idx uint8
  Value string
  FileValue string
  Selected string
}



type Kbgd struct {
  PageTitle string
  Client string
  DealDate string
  Train string
  Categories []Category
  Wagon string
  Seats string
  Meals []Meal
  TripDate string
  StartPlaces []StartPlace
  StartTime string
  Days []Day
  Phones string
  Salesmen []Salesman
  Tourists string
  Adults []int
  Children []int
  Lagguge []int
  Ferry []int
  Bus []int
  Breakfast bool
  Lunch bool
  Dinner bool
  Lunchbox bool
  Stamp bool
  Price string
}

type Fill struct {
  Client string
  DealDate string
  Train string
  Category string
  Wagon string
  Seats string
  TripDate string
  StartPlace string
  StartTime string
  Day string
  Phones string
  Salesman string
  Tourists string
  Adults string
  Children string
  Total string
  Lagguge string
  Ferry string
  Bus string
  Breakfast string
  Lunch string
  Dinner string
  Lunchbox string
  Stamp bool
  Price string
  FileEngSalesman string
}
type Result struct {
  PageTitle string
}

const (
  app_path = "/home/anton/golang/src/pdfgen/"
  font_path = "/home/anton/apps/go/src/github.com/jung-kurt/gofpdf/font"
  file_path = "pdf/kbgd/"
)
var kbgd = Kbgd{
  PageTitle: "Новый ваучер кругобайкалки",
  DealDate: "28.05.2015",
  Train: "6346",
  Categories: []Category{{1,"1 класс",""},{2,"2 класс",""}},
  Meals: []Meal{{0,"без питания",""},{1,"3-разовое питание",""}},
  TripDate: "06.06.2015",
  StartPlaces: []StartPlace{{0,"Вокзала",""},{1,"Драмтеатра",""}},
  StartTime: "08:10",
  Days: []Day{{0,"Воскресенье",""},{3,"Среда",""},{4,"Четверг",""},{5,"Пятница",""},{6,"Суббота",""}},
  Salesmen: []Salesman{{0,"Горщарук Екатерина","Gorscharuk-Kate",""},{1,"Гусева Мария","Guseva-Mariya",""},{2,"Червочкина Лидия","Chervochkina-Lidiya",""},{3,"Серышева Екатерина","Serusheva-Kate",""}},
  Adults: []int{1,2,3,4,5,6,7,8,9,10,11,12},
  Children: []int{0,1,2,3,4,5,6,7,8,9,10,11,12},
  Lagguge: []int{0,1,2,3,4,5,6,7,8,9,10},
  Ferry: []int{0,1,2,3,4,5,6,7,8,9,10,11,12},
  Bus: []int{0,1,2,3,4,5,6,7,8,9,10,11,12},
  Breakfast: false,
  Lunch: false,
  Dinner: false,
  Lunchbox: false,
  Stamp: false,
  Price: "7600 руб.",
  }
var counter int64 = 0

func processRequest(req *http.Request) (fill Fill) {
  var str string
  var total int64 = 0
  var idx int64
  var err error
  decoder := json.NewDecoder(req.Body)
  err = decoder.Decode(&fill)
  if (err != nil) {
    panic(fmt.Sprintf("Error while parsing json %v \n",err))
  } else {
    if idx,err = strconv.ParseInt(fill.StartPlace,10,8); err == nil {

      if idx == 0 {
        fill.StartPlace =  `от ж/д вокзала Иркутск-Пассажирский
ВНИМАНИЕ! Название поезда Иркутск - Слюдянка - Порт Байкал.
Быть за 15 минут до отправления!`
      } else {
        fill.StartPlace =  `от Драматического театра им. Охлопкова.
Сбор на площади перед театром (перекресток улиц 5-ой Армии и Карла Маркса).
Быть за 15 минут до отправления!`
      }
    }
    if idx,err = strconv.ParseInt(fill.Day,10,8); err == nil {
      if idx > 0 {
        idx -= 2
      }
      fill.Day = kbgd.Days[idx].Value
    }
    if idx,err = strconv.ParseInt(fill.Salesman,10,8); err == nil {
      fill.Salesman = kbgd.Salesmen[idx].Value
      fill.FileEngSalesman = kbgd.Salesmen[idx].FileValue
    }
    str = strings.Replace(fill.Tourists,"\n",", ",-1)
    str = strings.Replace(str,"\r","",-1)
    fill.Tourists = strings.TrimSpace(str)
    if idx,err = strconv.ParseInt(fill.Children,10,8); err == nil {
      total += idx
    } else {
      fill.Children = "0"
    }
    if idx,err = strconv.ParseInt(fill.Adults,10,8); err == nil {
      total += idx
    } else {
      panic(fmt.Sprintf("Invalid number of Adults %s",fill.Adults))
    }
    fill.Total = strconv.FormatInt(total,10)
    if idx,err = strconv.ParseInt(fill.Lagguge,10,8); err != nil {
      panic(fmt.Sprintf("Invalid number of Lagguge %s",fill.Lagguge))
    }
    if idx,err = strconv.ParseInt(fill.Ferry,10,8); err != nil {
      panic(fmt.Sprintf("Invalid number of Ferry %s",fill.Ferry))
    }
    if idx,err = strconv.ParseInt(fill.Bus,10,8); err != nil {
      panic(fmt.Sprintf("Invalid number of Bus %s",fill.Bus))
    }
    if fill.Breakfast == "on" {
      fill.Breakfast = "да"
    } else {
      fill.Breakfast = "нет"
    }
    if fill.Lunch == "on" {
      fill.Lunch = "да"
    } else {
      fill.Lunch = "нет"
    }
    if fill.Dinner == "on" {
      fill.Dinner = "да"
    } else {
      fill.Dinner = "нет"
    }
    if fill.Lunchbox == "on" {
      fill.Lunchbox = "да"
    } else {
      fill.Lunchbox = "нет"
    }
    return fill
  }
}

func handleKbgdDownload(writer http.ResponseWriter, request *http.Request,fileStr string){
  streamPDFbytes, err := ioutil.ReadFile(fileStr)
  if err != nil {
    log.Println("Downloading of file was failed",err)
    return
  }
  b := bytes.NewBuffer(streamPDFbytes)
  writer.Header().Set("Content-type","application/pdf")
  if _,err := b.WriteTo(writer); err !=nil {
    fmt.Fprintf(writer,"%s",err)
  }


}

func HandleKbgdData(writer http.ResponseWriter, request *http.Request){
  err := request.ParseForm()
  anError    := `<p class="error">%s</p>`
  if err != nil {
      fmt.Fprintf(writer,anError, err)
  }
  fill_pdf := processRequest(request)
  fileStr := createFKBGDTicket(fill_pdf)
  handleKbgdDownload(writer,request,fileStr)
}

func KbgdPage(writer http.ResponseWriter, request *http.Request) {
  htpl,err := ioutil.ReadFile(filepath.Join(filepath.Dir(os.Args[0]),"web/views/html/kbgd_vaucher.html"));
  if  err != nil {
    log.Printf("ReadFile error: %v",err)
  }

  tmpl,err := template.New("html").Parse(string(htpl))
  if err != nil {
        fmt.Printf("\nExecute error: %v", err)
        return
  }
  //fmt.Println(kbgd,tmpl,render.String())
  err = tmpl.Execute(writer,kbgd)
  if err != nil {
        fmt.Printf("\nExecute error: %v", err)
        return
  }

}


func createFKBGDTicket(fill Fill) string {
  pdf := gofpdf.New("P", "mm", "A4", font_path)
  pdf.AddFont("Arial","","Arial.json")
  pdf.AddFont("Arial_Bold","","Arial_Bold.json")
  pdf.SetTopMargin(15)
  pdf.SetAutoPageBreak(true,1.5)
  pdf.AddPage()
  //pdf.SetMargins(1,1,1)
  pdf.SetFont("Arial", "", 16)
  pdf.Image(filepath.Join(filepath.Dir(os.Args[0]),"images/trivium.png"), 140, 5, 60, 36.6, false, "", 0, "")

  str := `664011 г. Иркутск, ул. Ударника 7
тел. 8 (3952) 745-751, 8 800 775 45 51
e-mail: baikal@baikal.travel`
  pdf.MultiCell(110, 6, conv(str),"","LM",false)

  pdf.Ln(10)

  pdf.SetFont("Arial", "", 18)
  pdf.CellFormat(125,6,conv("Посадочный документ"),"",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 12)
  pdf.CellFormat(25,6,conv("дата:"),"",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(37,6,conv(fill.TripDate),"1",1,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(37,6,conv("Турагент: ООО \u0022КПТ\u0022"),"",1,"LM",false,0,"")

  pdf.CellFormat(190,6,conv("ФИО туристов: " + fill.Tourists),"",1,"LM",false,0,"")
  pdf.CellFormat(190,6,conv("Список туристов. Всего: " + fill.Total+"; Взрослых: " + fill.Adults + "; Детей: " + fill.Children + "."),"",1,"LM",false,0,"")
  pdf.Ln(2)
  pdf.CellFormat(15,6,conv("Вагон "),"",0,"LM",false,0,"")
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(12,6,conv(fill.Wagon),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  str = "Места "
  if idx := strings.Index(fill.Seats,","); idx == -1 {
    str = "Место "
  }
  pdf.CellFormat(20,6,conv(str),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(96,6,conv(fill.Seats),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(15,6,conv("1 кл. "),"",0,"RM",false,0,"")

  if fill.Category == "1" {
    str = "+"
  } else {
    str = ""
  }
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(7,6,conv(str),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(15,6,conv("2 кл. "),"",0,"RM",false,0,"")

  if fill.Category == "2" {
    str = "+"
  } else {
    str = " "
  }
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(7,6,conv(str),"1",1,"CM",false,0,"")

  pdf.Ln(3)

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(13,6,conv("Багаж "),"",0,"LM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Lagguge),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(35,6,conv("Водный транспорт "),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Ferry),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(19,6,conv("Автобус "),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Bus),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(18,6,conv("Завтрак "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Breakfast),"1",0,"CM",false,0,"")

  pdf.CellFormat(14,6,conv("Обед "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Lunch),"1",0,"CM",false,0,"")

  pdf.CellFormat(14,6,conv("Ужин "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Dinner),"1",0,"CM",false,0,"")

  pdf.CellFormat(21,6,conv("Lunch box "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Lunchbox),"1",1,"CM",false,0,"")


  if fill.Price != "" {
    pdf.Ln(3)
    pdf.CellFormat(24,6,conv("Стоимость: "),"",0,"LM",false,0,"")
    pdf.CellFormat(96,6,conv(fill.Price),"",1,"LM",false,0,"")
  }
  pdf.Ln(3)

  pdf.CellFormat(24,6,conv("Продавец: "),"",0,"LM",false,0,"")
  pdf.CellFormat(60,6,conv(fill.Salesman),"",0,"LM",false,0,"")

  pdf.CellFormat(26,6,conv("Покупатель: "),"",0,"LM",false,0,"")
  pdf.CellFormat(78,6,conv(fill.Client),"",1,"LM",false,0,"")

  pdf.Ln(5)

  pdf.CellFormat(26,6,conv("М. П. Турагента"),"",1,"LM",false,0,"")

  pdf.Ln(3)

  pdf.CellFormat(30,6,conv("Дата продажи: "),"",0,"LM",false,0,"")
  pdf.CellFormat(38,6,conv(fill.DealDate),"",0,"LM",false,0,"")

  pdf.CellFormat(45,6,conv("Телефон покупателя: "),"",0,"LM",false,0,"")
  pdf.CellFormat(70,6,conv(fill.Phones),"",1,"LM",false,0,"")

  pdf.Ln(3)



  pdf.CellFormat(35,6,conv(fill.Day),"",0,"LM",false,0,"")

  pdf.CellFormat(17,6,conv("Поезд: "),"",0,"LM",false,0,"")
  pdf.CellFormat(16,6,conv(fill.Train),"",0,"LM",false,0,"")

  pdf.SetFont("Arial_Bold", "", 10)
  pdf.CellFormat(32,6,conv("Отправление: "),"",0,"LM",false,0,"")

  pdf.CellFormat(16,6,conv(fill.StartTime),"",1,"LM",false,0,"")
  pdf.MultiCell(190, 6, conv(fill.StartPlace),"","LM",false)

  pdf.Ln(2)
  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(33,6,conv("Возвращение: "),"",0,"LM",false,0,"")
  pdf.CellFormat(90,6,conv("ж/д вокзал Иркутск-Пассажирский"),"",1,"LM",false,0,"")

  pdf.Ln(4)

  pdf.Line(pdf.GetX(),pdf.GetY(),pdf.GetX()+190,pdf.GetY())
  pdf.Ln(5)
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////

  pdf.SetFont("Arial", "", 18)
  pdf.CellFormat(125,6,conv("Посадочный документ"),"",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 12)
  pdf.CellFormat(25,6,conv("дата:"),"",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(37,6,conv(fill.TripDate),"1",1,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(37,6,conv("Турагент: ООО \u0022КПТ\u0022"),"",1,"LM",false,0,"")

  pdf.CellFormat(190,6,conv("ФИО туристов: " + fill.Tourists),"",1,"LM",false,0,"")
  pdf.CellFormat(190,6,conv("Список туристов. Всего: " + fill.Total+"; Взрослых: " + fill.Adults + "; Детей: " + fill.Children + "."),"",1,"LM",false,0,"")
  pdf.Ln(2)
  pdf.CellFormat(15,6,conv("Вагон "),"",0,"LM",false,0,"")
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(12,6,conv(fill.Wagon),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  str = "Места "
  if idx := strings.Index(fill.Seats,","); idx == -1 {
    str = "Место "
  }
  pdf.CellFormat(20,6,conv(str),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(96,6,conv(fill.Seats),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(15,6,conv("1 кл. "),"",0,"RM",false,0,"")

  if fill.Category == "1" {
    str = "+"
  } else {
    str = ""
  }
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(7,6,conv(str),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(15,6,conv("2 кл. "),"",0,"RM",false,0,"")

  if fill.Category == "2" {
    str = "+"
  } else {
    str = " "
  }
  pdf.SetFont("Arial", "", 16)
  pdf.CellFormat(7,6,conv(str),"1",1,"CM",false,0,"")

  pdf.Ln(3)

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(13,6,conv("Багаж "),"",0,"LM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Lagguge),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(35,6,conv("Водный транспорт "),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Ferry),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(19,6,conv("Автобус "),"",0,"RM",false,0,"")

  pdf.SetFont("Arial", "", 14)
  pdf.CellFormat(7,6,conv(fill.Bus),"1",0,"CM",false,0,"")

  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(18,6,conv("Завтрак "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Breakfast),"1",0,"CM",false,0,"")

  pdf.CellFormat(14,6,conv("Обед "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Lunch),"1",0,"CM",false,0,"")

  pdf.CellFormat(14,6,conv("Ужин "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Dinner),"1",0,"CM",false,0,"")

  pdf.CellFormat(21,6,conv("Lunch box "),"",0,"RM",false,0,"")
  pdf.CellFormat(8,6,conv(fill.Lunchbox),"1",1,"CM",false,0,"")

  if fill.Price != "" {
    pdf.Ln(3)
    pdf.CellFormat(24,6,conv("Стоимость: "),"",0,"LM",false,0,"")
    pdf.CellFormat(96,6,conv(fill.Price),"",1,"LM",false,0,"")
  }
  pdf.Ln(3)

  pdf.CellFormat(24,6,conv("Продавец: "),"",0,"LM",false,0,"")
  pdf.CellFormat(60,6,conv(fill.Salesman),"",0,"LM",false,0,"")

  pdf.CellFormat(26,6,conv("Покупатель: "),"",0,"LM",false,0,"")
  pdf.CellFormat(78,6,conv(fill.Client),"",1,"LM",false,0,"")

  pdf.Ln(4)

  pdf.CellFormat(26,6,conv("М. П. Турагента"),"",1,"LM",false,0,"")

  pdf.Ln(3)

  pdf.CellFormat(30,6,conv("Дата продажи: "),"",0,"LM",false,0,"")
  pdf.CellFormat(38,6,conv(fill.DealDate),"",0,"LM",false,0,"")

  pdf.CellFormat(45,6,conv("Телефон покупателя: "),"",0,"LM",false,0,"")
  pdf.CellFormat(70,6,conv(fill.Phones),"",1,"LM",false,0,"")

  pdf.Ln(3)



  pdf.CellFormat(35,6,conv(fill.Day),"",0,"LM",false,0,"")

  pdf.CellFormat(17,6,conv("Поезд: "),"",0,"LM",false,0,"")
  pdf.CellFormat(16,6,conv(fill.Train),"",0,"LM",false,0,"")

  pdf.SetFont("Arial_Bold", "", 10)
  pdf.CellFormat(32,6,conv("Отправление: "),"",0,"LM",false,0,"")

  pdf.CellFormat(16,6,conv(fill.StartTime),"",1,"LM",false,0,"")
  pdf.MultiCell(190, 6, conv(fill.StartPlace),"","LM",false)

  pdf.Ln(2)
  pdf.SetFont("Arial", "", 10)
  pdf.CellFormat(33,6,conv("Возвращение: "),"",0,"LM",false,0,"")
  pdf.CellFormat(90,6,conv("ж/д вокзал Иркутск-Пассажирский"),"",1,"LM",false,0,"")

  if fill.Stamp {
    var stampX float64 = 13
    var stampY1 float64 = 90
    var stampY2 float64 = 213
    if fill.Price == "" {
      stampY1 = 85
      stampY2 = 199
    }
    pdf.Image(filepath.Join(filepath.Dir(os.Args[0]),"images/stamp3.png"), stampX, stampY1, 0, 0, false, "", 0, "")
    pdf.Image(filepath.Join(filepath.Dir(os.Args[0]),"images/stamp3.png"), stampX, stampY2, 0, 0, false, "", 0, "")
  }


  fileStr := filepath.Join( filepath.Dir(os.Args[0]), genFileName(fill.FileEngSalesman)+".pdf")
  err := pdf.OutputFileAndClose(fileStr)
  if err == nil {
    fmt.Println("Successfully generated",fileStr)
    return fileStr
  } else {
    fmt.Println(err)
    return ""
  }
}

func conv(str string) string {
  str, _ = iconv.ConvertString(str, "utf-8", "iso8859-5")
  return str
}
func genFileName(manager string) string {
  counter += 1
  return file_path+manager+"-"+strconv.FormatInt(int64(time.Now().Unix()),10)+"-"+strconv.FormatInt(counter,10)
}
